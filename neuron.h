#ifndef NEURON_H
#define NEURON_H

#include <QVector>

class Neuron {
public:
    QVector<double> weights;

    Neuron();
    Neuron(int inputs);

    void reinit();
};

#endif // NEURON_H
