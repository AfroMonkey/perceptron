#include "neuron.h"

#include <QRandomGenerator>

Neuron::Neuron() {
}

Neuron::Neuron(int inputs) {
    weights.resize(inputs + 1);
    reinit();
}

void Neuron::reinit() {
    for (auto i = 0; i < weights.length(); ++i) {
        weights[i] = QRandomGenerator::global()->generateDouble() * 2 - 1;
    }
}
