#ifndef PERCEPTRON_H
#define PERCEPTRON_H

#include "neuron.h"

#include <QPointF>

class Perceptron {
public:
    Neuron neuron;
    QVector<QPair<QVector<double>, double>> training_data;
    double eta;
    bool done;

    Perceptron();
    Perceptron(int inputs);

    void reinit();
    int next_epoch();
    double eval(QVector<double> point);
    double f(double value);
};

#endif // PERCEPTRON_H
