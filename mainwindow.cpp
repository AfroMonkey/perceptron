#include "mainwindow.h"

#include "ui_mainwindow.h"

#define X_AXIS 10
#define Y_AXIS 10
#define X_STEP 0.1
#define Y_STEP 0.1

MainWindow::MainWindow(QWidget* parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow) {
    ui->setupUi(this);

    perceptron = Perceptron(2);
    training = false;

    ui->points_list->setModel(new QStringListModel(this));
    ui->epochs_spin->setValue(100);

    prepare_plot();

    connect(ui->main_plot, &QCustomPlot::mouseDoubleClick, this, &MainWindow::main_plot_double_clicked);

    QTimer* timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &MainWindow::train);
    timer->start(10);
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::prepare_plot() {
    ui->main_plot->addGraph();
    ui->main_plot->addGraph();
    ui->main_plot->addGraph();
    ui->main_plot->addGraph();
    ui->main_plot->addGraph();
    ui->main_plot->addGraph();
    ui->main_plot->xAxis->setRange(-X_AXIS, X_AXIS);
    ui->main_plot->yAxis->setRange(-Y_AXIS, Y_AXIS);
    ui->main_plot->graph(0)->setLineStyle(QCPGraph::lsNone);
    ui->main_plot->graph(0)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, 5));
    ui->main_plot->graph(1)->setLineStyle(QCPGraph::lsNone);
    ui->main_plot->graph(1)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCross, Qt::red, 5));
    ui->main_plot->graph(2)->setPen(QPen(QBrush(Qt::green), 5));
    ui->main_plot->graph(3)->setPen(QPen(Qt::black));
    ui->main_plot->graph(4)->setLineStyle(QCPGraph::lsNone);
    ui->main_plot->graph(4)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, 1));
    ui->main_plot->graph(5)->setLineStyle(QCPGraph::lsNone);
    ui->main_plot->graph(5)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCross, Qt::red, 1));
    ui->main_plot->replot();

    ui->errors_plot->addGraph();
    ui->errors_plot->xAxis->setLabel("Epochs");
    ui->errors_plot->yAxis->setLabel("Errors");
}

void MainWindow::main_plot_double_clicked(QMouseEvent* e) {
    QPointF point = QPointF(ui->main_plot->xAxis->pixelToCoord(e->x()), ui->main_plot->yAxis->pixelToCoord(e->y()));
    double type = 0;
    switch (e->button()) {
        case Qt::LeftButton:
            type = 0;
            break;
        case Qt::RightButton:
            type = 1;
            break;
        default:
            break;
    }
    QVector<double> point_v;
    point_v.push_back(static_cast<double>(point.x()));
    point_v.push_back(static_cast<double>(point.y()));
    perceptron.training_data.push_back(QPair<QVector<double>, double>(point_v, type));

    QStringListModel* m = static_cast<QStringListModel*>(ui->points_list->model());
    m->insertRow(m->rowCount());
    m->setData(m->index(m->rowCount() - 1, 0), QString::number(type) + " " + QString::number(point.x()) + ", " + QString::number(point.y()));

    ui->main_plot->graph(static_cast<int>(type))->addData(point.x(), point.y());
    ui->main_plot->replot();
}

void MainWindow::main_plot_clear(int from = 0) {
    for (auto g = from; g < ui->main_plot->graphCount(); ++g) {
        ui->main_plot->graph(g)->setData(QVector<double>(), QVector<double>());
    }
    ui->main_plot->replot();
}

void MainWindow::on_reset_button_clicked() {
    perceptron.training_data.clear();

    static_cast<QStringListModel*>(ui->points_list->model())->removeRows(0, static_cast<QStringListModel*>(ui->points_list->model())->rowCount());
    clear_ui();
    main_plot_clear();
    errors_plot_clear();
    training = false;
}

void MainWindow::on_reinit_button_clicked() {
    perceptron.reinit();
    main_plot_clear(2);
    errors_plot_clear();
    plot_perceptron(1);
    clear_ui();
    ui->start_button->setEnabled(true);
    training = false;
}

void MainWindow::plot_perceptron(int layer = 0) {
    auto a = perceptron.neuron.weights[0];
    auto b = perceptron.neuron.weights[1];
    auto c = perceptron.neuron.weights[2];

    ui->w0Spin->setValue(a);
    ui->w1Spin->setValue(b);
    ui->w2Spin->setValue(c);

    ui->main_plot->graph(2 + layer)->setData(QVector<double>(), QVector<double>());
    ui->main_plot->graph(2 + layer)->addData(-(b * 10 + c) / a, 10);
    ui->main_plot->graph(2 + layer)->addData(-(b * -10 + c) / a, -10);
    ui->main_plot->replot();
}

void MainWindow::on_start_button_clicked() {
    training = true;
    set_enabled_ui(false);

    ui->errors_plot->xAxis->setRange(0, ui->epochs_spin->value());
    ui->errors_plot->yAxis->setRange(0, perceptron.training_data.length());
}

void MainWindow::on_eta_slider_valueChanged(int value) {
    perceptron.eta = static_cast<double>(value) / ui->eta_slider->maximum();
    ui->eta_label->setText(QString::number(perceptron.eta));
}

void MainWindow::train() {
    if (training) {
        errors.append(perceptron.next_epoch());
        plot_perceptron();
        plot_error(errors.last(), ui->epochs_used_spin->value());
        ui->epochs_used_spin->stepUp();

        if (perceptron.done or ui->epochs_used_spin->value() >= ui->epochs_spin->value()) {
            if (perceptron.done) {
                QMessageBox(QMessageBox::Information, "Done", "The perceptron has converged.").exec();
            } else {
                QMessageBox(QMessageBox::Warning, "Error", "The perceptron has not converged.").exec();
            }
            training = false;
            ui->start_button->setEnabled(false);
            plot_all_space();
        }
    }
}

void MainWindow::plot_all_space() {
    for (double x = -X_AXIS; x < X_AXIS; x += X_STEP) {
        for (double y = -Y_AXIS; y < Y_AXIS; y += Y_STEP) {
            QVector<double> point;
            point.append(x);
            point.append(y);
            point.append(1);
            if (perceptron.f(perceptron.eval(point)) > 0) {
                ui->main_plot->graph(5)->addData(point[0], point[1]);
            } else {
                ui->main_plot->graph(4)->addData(point[0], point[1]);
            }
        }
    }
    ui->main_plot->replot();
}

void MainWindow::plot_error(int errors, int epoch) {
    ui->errors_plot->graph(0)->addData(epoch, errors);
    ui->errors_plot->replot();
}

void MainWindow::errors_plot_clear() {
    ui->errors_plot->graph(0)->setData(QVector<double>(), QVector<double>());
    ui->errors_plot->replot();
}

void MainWindow::clear_ui() {
    ui->start_button->setEnabled(false);
    set_enabled_ui(true);
    ui->epochs_used_spin->setValue(0);
}

void MainWindow::set_enabled_ui(bool state) {
    ui->eta_slider->setEnabled(state);
    ui->epochs_spin->setEnabled(state);
    ui->w0Spin->setEnabled(state);
    ui->w1Spin->setEnabled(state);
    ui->w2Spin->setEnabled(state);
}

void MainWindow::on_w0Spin_valueChanged(double w0) {
    if (training) return;
    perceptron.neuron.weights[0] = w0;
    plot_perceptron(1);
}

void MainWindow::on_w1Spin_valueChanged(double w1) {
    if (training) return;
    perceptron.neuron.weights[1] = w1;
    plot_perceptron(1);
}

void MainWindow::on_w2Spin_valueChanged(double w2) {
    if (training) return;
    perceptron.neuron.weights[2] = w2;
    plot_perceptron(1);
}
