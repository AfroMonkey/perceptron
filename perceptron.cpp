#include "perceptron.h"

Perceptron::Perceptron() {
    done = false;
}

Perceptron::Perceptron(int size) {
    done = false;
    eta = 0.5;
    neuron = Neuron(size);
}

void Perceptron::reinit() {
    neuron.reinit();
}

int Perceptron::next_epoch() {
    int errors = 0;
    done = true;
    for (auto t : training_data) {
        t.first.append(1);
        auto error = t.second - f(eval(t.first));
        if (error) {
            done = false;
            ++errors;
            for (auto d = 0; d < neuron.weights.length(); ++d) {
                neuron.weights[d] += eta * error * t.first[d];
            }
        }
    }
    return errors;
}

double Perceptron::eval(QVector<double> point) {
    double e = 0;
    for (auto d = 0; d < neuron.weights.length(); ++d) {
        e += neuron.weights[d] * point[d];
    }
    return e;
}

double Perceptron::f(double value) {
    return value >= 0 ? 1 : 0;
}
