#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "perceptron.h"
#include "qcustomplot.h"

#include <QMainWindow>

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit MainWindow(QWidget* parent = nullptr);
    ~MainWindow();

private slots:
    void main_plot_double_clicked(QMouseEvent* e);
    void train();
    void on_reset_button_clicked();
    void on_reinit_button_clicked();
    void on_start_button_clicked();
    void on_eta_slider_valueChanged(int value);

    void on_w0Spin_valueChanged(double arg1);

    void on_w1Spin_valueChanged(double arg1);

    void on_w2Spin_valueChanged(double arg1);

private:
    Ui::MainWindow* ui;
    Perceptron perceptron;
    QVector<int> errors;
    bool training;

    void prepare_plot();
    void plot_perceptron(int layer);
    void plot_all_space();
    void plot_error(int errors, int epoch);
    void main_plot_clear(int from);
    void errors_plot_clear();
    void clear_ui();
    void set_enabled_ui(bool state);
};

#endif // MAINWINDOW_H
